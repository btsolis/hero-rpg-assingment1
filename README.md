
# CONSOLE HERO RPG

Sumbission for Assignment 1. A simple RPG running on console as required by the
documentation. No extra functionality from Appendix D has been met. 

All other  code functionality except Display() testing has been met.

No CI pipeline for testing exists/works. 

## What I used
I used Visual Studio 2022 Community edition as the IDE and the project was built with 
.NET6.

Beyond the normal C# libraries I also downloaded and used the DeepEqual library
(https://www.nuget.org/packages/DeepEqual/#readme-body-tab) for testing. 

## What I Learned
I think my main gain from this assignment was learning how to unit test. It is something I had not
done before and I feel like it helped improve my process and also the speed at which I could 
spot and correct any logical or syntax mistakes I made. 

Also in general it was good practice for my OOP skills. 

## Index

Assignment1_RPGHeroes contains the main project with the codebase while
HeroRPGTests is the secondary project with the unit testing. 

## How to run

As required by the assignment doc, there is no actual program running in the main method
and the assignment is run by testing. 

To initiate testing, you must download/clone the repo and open the solution(Assingment1_RPGHeroes.sln) in your
Visual Studio installation. In the solution explorer you should see both the projects 
for the actual assignment and the project for testing. 
You can proceed to run the tests from the Test Explorer at your leisure. 

IMPORTANT: DeepEqual library must be installed. To easily install it via Visual Studio
go Tools>Manage NuGet Packages for Solution... and search "Deep Equal" on the search bar. 
Alternatively you can download it manually from the link provided in the "What I used" section

## General Notes Regarding Testing

You will notice that most of the tests revolve around testing the Mage class. 
This was done for conveniece since all 4 subclasses work identically. Creating tests for 
all 4 classes in all scenarios would be both very time consuming and tedious. 
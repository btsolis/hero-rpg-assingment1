﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assingment1_RPGHeroes.Exceptions
{
    public class InvalidArmorException : Exception
    {
        //thrown when you try to equip an incompatible armor
        public InvalidArmorException() : base() { }
        public InvalidArmorException(string msg) : base(msg) { }
        public InvalidArmorException(string msg, Exception inner) : base(msg, inner) { }
    }
}

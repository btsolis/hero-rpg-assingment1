﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assingment1_RPGHeroes.Exceptions
{
    public class InvalidWeaponException : Exception
    {
        //thrown when you try to equip an incompatible weapons
        public InvalidWeaponException() : base() { }
        public InvalidWeaponException(string msg) : base(msg) { }
        public InvalidWeaponException(string msg, Exception inner) : base(msg, inner) { }

    }
}

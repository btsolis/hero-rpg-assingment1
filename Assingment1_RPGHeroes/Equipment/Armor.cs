﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace Assingment1_RPGHeroes.Equipment
{
    public class Armor : Item
    {
        //armor fields
        public enum ArmorType 
        { 
            Cloth, 
            Leather, 
            Mail, 
            Plate,

        };
        
        public ArmorType Type { get; set; }

        public HeroAttribute ArmorAttribute { get; set; }

        public Armor(string name, ArmorType type, Slot slot, int str, int dex, int intel, int level)
        {
            //robust constructor. can create any kind of armor desired
            Name = name;
            Type = type;
            EquipSlot= slot;
            RequiredLevel = level;
            HeroAttribute stats = new HeroAttribute();
            stats.ChangeAtts(str, dex, intel);
            ArmorAttribute = stats;
            
        }
    }
}

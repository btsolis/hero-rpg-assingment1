﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assingment1_RPGHeroes.Equipment
{
    public abstract class Item
    {
        //only contains the very necessary fields for an Item
        public enum Slot 
        { 
            Weapon, 
            Head, 
            Body, 
            Legs 
        }

        public Slot EquipSlot { get; set; }
        public string Name { get; set; }
        public int RequiredLevel { get; set; }

       
    }
}

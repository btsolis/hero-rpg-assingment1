﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assingment1_RPGHeroes.Equipment
{
    public class Weapon : Item
    {
        //weapon fields
        public enum WeaponType 
        { 
            Axe, 
            Bow, 
            Dagger, 
            Hammer, 
            Staff, 
            Sword, 
            Wand 
        };

        public WeaponType Type { get; set; }
        public int WeaponDamage { get; set; }



        public Weapon(string name, int damage, WeaponType type, int lvlReq)
        {
            //robust weapon constructor. can create any weapon you desire
            Name = name;
            Type = type;
            RequiredLevel= lvlReq;
            WeaponDamage = damage;
            EquipSlot = Slot.Weapon;                            

        }



    }

}

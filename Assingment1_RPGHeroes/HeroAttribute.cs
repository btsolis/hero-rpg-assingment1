﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assingment1_RPGHeroes
{
    public class HeroAttribute
    {
        //attributes
        public int Strength { get; set; }  //determines physical strength
        public int Dexterity { get; set; } //determines ability to attack with speed and nimblenss
        public int Intelligence { get; set; } //determines affinity with magic

        public void ChangeAtts(int strGain, int dexGain, int intGain)
        {
            //updates each attribute by its given ammount. works for negatives numbers as well. 
            this.Strength += strGain;
            this.Dexterity += dexGain;
            this.Intelligence += intGain;
            
        }
    }
}

﻿using Assingment1_RPGHeroes.Equipment;
using Assingment1_RPGHeroes.Exceptions;
using Assingment1_RPGHeroes.NewFolder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assingment1_RPGHeroes
{
    public abstract class Hero
    {
        

        //shared fields as properties
        public string Name { get; set; }
        public int Level { get; set; }      
        
        public HeroAttribute LevelAttributes { get; set; }

        public Dictionary<Item.Slot, Armor?> Equipment = new Dictionary<Item.Slot, Armor?>
            {
                //{Item.Slot.Weapon, null },
                {Item.Slot.Head, null },
                {Item.Slot.Body, null },
                {Item.Slot.Legs, null },
            };

        public Weapon EquippedWeapon { get; set; }

        public List<Weapon.WeaponType> ValidWeapons { get; set; }

        public List<Armor.ArmorType> ValidArmors { get; set; }







        //constructor
        public Hero(string name)
        {
            Name = name;
            Level = 1;
        }

        //public facing methods
        public void LevelUp(int strGain, int dexGain, int intGain)
        {
            //receives the levelup incements from a subclass and levels up the hero
            this.Level += 1;
            HeroAttribute heroAtt = new HeroAttribute();
            heroAtt.ChangeAtts(strGain, dexGain, intGain);
            this.LevelAttributes.Strength += heroAtt.Strength;
            this.LevelAttributes.Dexterity += heroAtt.Dexterity;
            this.LevelAttributes.Intelligence += heroAtt.Intelligence;
        }

        
        public void Equip(Weapon weapon)
        {
            //used for equipping weapons. checks for appropriate level and weapon type and throws exception if incorrect. 
            
                if ((weapon.RequiredLevel <= Level) &&  ValidWeapons.Contains(weapon.Type))
                {
                    EquippedWeapon = weapon;
                }                    
                else
                {
                    //throw custom wep exc
                    throw new InvalidWeaponException("This weapon cannot be equipped");
                }           

        }

        public void Equip(Armor armor)
        {
            //used for equipping armos. checks for appropriate level and armor type and throws exception if incorrect. 

            if ((armor.RequiredLevel <= Level) && ValidArmors.Contains(armor.Type))
            {
                Equipment[armor.EquipSlot] = armor;
            }
            else
            {
                //throw custom wep exc
                throw new InvalidArmorException("This weapon cannot be equipped");
            }

        }

        public int Damage(int damagingAttribute)
        {
            //calculates and returns damage as an integer
            int weaponDamage;
            if (EquippedWeapon == null)
            {
                weaponDamage = 1;
            }
            else
            {
                weaponDamage = EquippedWeapon.WeaponDamage;
            }


            int dmg = weaponDamage * (1 + damagingAttribute / 100);

            return dmg;
        }

        public HeroAttribute TotalAttributes()
        {
            //calculates total attributes and returns the values as a HeroAttribute object

            HeroAttribute totalAtts = new HeroAttribute();
            totalAtts.Dexterity = 0;
            totalAtts.Intelligence = 0;
            totalAtts.Strength = 0;
            totalAtts.ChangeAtts(LevelAttributes.Strength, LevelAttributes.Dexterity, LevelAttributes.Intelligence);

            
            for (int index = 0; index < Equipment.Count; index++)
            {
                var entry = Equipment.ElementAt(index);
                if (entry.Value != null)
                {
                    Armor armor = entry.Value;
                    totalAtts.ChangeAtts(armor.ArmorAttribute.Strength,armor.ArmorAttribute.Dexterity,armor.ArmorAttribute.Intelligence);
                }
            }

            return totalAtts;

        }

        public void Display()
        {
            //displays the hero's state on the console. works but i don't know to unit test it without refactoring spefically to allow for unit testing
            
            HeroAttribute totalAtts = new HeroAttribute();
            totalAtts = this.TotalAttributes();
            string displayMsg = "The Hero's name is " + Name + "and their Level is" + Level + "." +
                "\nAt their current level their base attributes are:" +
                "\nStrength: " + LevelAttributes.Strength +
                "\nDexterity: " + LevelAttributes.Dexterity +
                "\nIntelligence: " + LevelAttributes.Intelligence + "" +
                "\n\n" +
                "Furthermore they have the following equipment:" +
                "\nWeapon: " + EquippedWeapon.Name +
                "\nHead: " + Equipment[Item.Slot.Head].Name +
                "\nBody: " + Equipment[Item.Slot.Body].Name +
                "\nLegs: " + Equipment[Item.Slot.Legs].Name +
                "\n\nTheir equipment augment their attributes. This means their total attributes are as follows:" +
                "\nStrength: " + totalAtts.Strength +
                "\nDexterity: " + totalAtts.Dexterity +
                "\nIntelligence: " + totalAtts.Intelligence + "";
            Console.WriteLine(displayMsg);
            Console.ReadLine();


        }
                


    }
}

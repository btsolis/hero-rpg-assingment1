﻿using Assingment1_RPGHeroes.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assingment1_RPGHeroes.HeroClasses
{
    public class Ranger : Hero
    {
        //properties
        int strGainOnLvlUp = 1;
        int dexGainOnLvlUp = 5;
        int intGainOnLvlUp = 1;

        private List<Weapon.WeaponType> rangerWeapons = new List<Weapon.WeaponType>()
        {
            Weapon.WeaponType.Bow
        };

        private List<Armor.ArmorType> rangerArmor = new List<Armor.ArmorType>()
        {
            Armor.ArmorType.Leather,
            Armor.ArmorType.Mail,
        };


        public Ranger(string name) : base(name)
        {//Ranger constructor
            Name = name;
            Level = 1;
            HeroAttribute attributes = new HeroAttribute();
            attributes.Strength = 1;
            attributes.Dexterity = 7;
            attributes.Intelligence = 1;
            LevelAttributes = attributes;
            ValidArmors = rangerArmor;
            ValidWeapons = rangerWeapons;
            EquippedWeapon = null;

        }

        public void RangerLevel()
        {//passes lvl up increments to LevelUp
            this.LevelUp(strGainOnLvlUp, dexGainOnLvlUp, intGainOnLvlUp);
        }

        public int Damage()
        {//calculates and passes damaging attribute to main dmg method on Hero
            HeroAttribute dmgAtt = new HeroAttribute();
            dmgAtt = this.TotalAttributes();
            int dmg = this.Damage(dmgAtt.Dexterity);

            return dmg;
        }
    }
}

﻿using Assingment1_RPGHeroes.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assingment1_RPGHeroes.HeroClasses
{
    
    public class Rogue : Hero
    {
        //properties
        int strGainOnLvlUp = 1;
        int dexGainOnLvlUp = 4;
        int intGainOnLvlUp = 1;

        private List<Weapon.WeaponType> rogueWeapons = new List<Weapon.WeaponType>()
        {
            Weapon.WeaponType.Dagger,
            Weapon.WeaponType.Sword
        };

        private List<Armor.ArmorType> rogueArmor = new List<Armor.ArmorType>()
        {
            Armor.ArmorType.Leather,
            Armor.ArmorType.Mail,
        };


        public Rogue(string name) : base(name)
        {//Rogue constructor
            Name = name;
            Level = 1;
            HeroAttribute attributes = new HeroAttribute();
            attributes.Strength = 2;
            attributes.Dexterity = 6;
            attributes.Intelligence = 1;
            LevelAttributes = attributes;
            ValidArmors = rogueArmor;
            ValidWeapons = rogueWeapons;
            EquippedWeapon = null;

        }

        public void RogueLevel()
        {//passes lvl up increments to LevelUp
            this.LevelUp(strGainOnLvlUp, dexGainOnLvlUp, intGainOnLvlUp);
        }

        public int Damage()
        {//calculates and passes damaging attribute to main dmg method on Hero
            HeroAttribute dmgAtt = new HeroAttribute();
            dmgAtt = this.TotalAttributes();
            int dmg = this.Damage(dmgAtt.Dexterity);

            return dmg;
        }
    }
}

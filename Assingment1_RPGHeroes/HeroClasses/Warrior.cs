﻿using Assingment1_RPGHeroes.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assingment1_RPGHeroes.HeroClasses
{
    public class Warrior : Hero
    {
        //properties
        int strGainOnLvlUp = 3;
        int dexGainOnLvlUp = 2;
        int intGainOnLvlUp = 1;

        private List<Weapon.WeaponType> warriorWeapons = new List<Weapon.WeaponType>()
        {
            Weapon.WeaponType.Axe,
            Weapon.WeaponType.Hammer,
            Weapon.WeaponType.Sword,
        };

        private List<Armor.ArmorType> warriorArmor = new List<Armor.ArmorType>()
        {
            Armor.ArmorType.Mail,
            Armor.ArmorType.Plate
        };


        public Warrior(string name) : base(name)
        {//Ranger constructor
            Name = name;
            Level = 1;
            HeroAttribute attributes = new HeroAttribute();
            attributes.Strength = 5;
            attributes.Dexterity = 2;
            attributes.Intelligence = 1;
            LevelAttributes = attributes;
            ValidArmors = warriorArmor;
            ValidWeapons = warriorWeapons;
            EquippedWeapon = null;

        }

        public void WarriorLevel()
        {//passes lvl up increments to LevelUp
            this.LevelUp(strGainOnLvlUp, dexGainOnLvlUp, intGainOnLvlUp);
        }

        public int Damage()
        {//calculates and passes damaging attribute to main dmg method on Hero
            HeroAttribute dmgAtt = new HeroAttribute();
            dmgAtt = this.TotalAttributes();
            int dmg = this.Damage(dmgAtt.Dexterity);

            return dmg;
        }
    }
}

﻿using Assingment1_RPGHeroes.Equipment;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assingment1_RPGHeroes.NewFolder
{
    public class Mage : Hero
    {
        //properties
        int strGainOnLvlUp = 1;
        int dexGainOnLvlUp = 1;
        int intGainOnLvlUp = 5;       

        private List<Weapon.WeaponType> mageWeapons = new List<Weapon.WeaponType>()
        {
            Weapon.WeaponType.Staff,
            Weapon.WeaponType.Wand
        };

        private List<Armor.ArmorType> mageArmor = new List<Armor.ArmorType>()
        {
            Armor.ArmorType.Cloth
        };


        public Mage(string name) : base(name)
        {//Mage constructor
            Name = name;
            Level = 1;
            HeroAttribute attributes = new HeroAttribute();
            attributes.Strength = 1;
            attributes.Dexterity = 1;
            attributes.Intelligence = 8;
            LevelAttributes = attributes;
            ValidArmors = mageArmor;
            ValidWeapons= mageWeapons;
            EquippedWeapon = null;

        }

        public void MageLevel()
        {//passes lvl up increments to LevelUp
            this.LevelUp(strGainOnLvlUp, dexGainOnLvlUp, intGainOnLvlUp);
        }

        public int Damage()
        {//calculates and passes damaging attribute to main dmg method on Hero
            HeroAttribute dmgAtt= new HeroAttribute();
            dmgAtt = this.TotalAttributes();
            int dmg = this.Damage(dmgAtt.Intelligence);

            return dmg;
        }
                

    }
}

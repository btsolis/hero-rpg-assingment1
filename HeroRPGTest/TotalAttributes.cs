﻿using Assingment1_RPGHeroes.Equipment;
using Assingment1_RPGHeroes.NewFolder;
using DeepEqual.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroRPGTest
{
    public class TotalAttributes
    {
        [Fact]
        public void TotalAtt_NoEquip_TestIfTotalAttributesAreCorrectWithNoEquipment()
        {
            //Assemble
            Mage mage = new Mage("test");
            HeroAttribute actualAtts = new HeroAttribute();
            //Act
            actualAtts = mage.TotalAttributes();

            //Assert
            mage.LevelAttributes.ShouldDeepEqual(actualAtts);

        }

        [Fact]
        public void TotalAtt_OneEquip_TestIfTotalAttributesAreCorrectWithOneEquipment()
        {
            //Assemble
            Mage mage = new Mage("test");
            HeroAttribute actualAtts = new HeroAttribute();
            HeroAttribute expectedAtts = new HeroAttribute();
            Armor armor = new Armor("common robes", Armor.ArmorType.Cloth, Item.Slot.Body, 0, 0, 5, 1);
            expectedAtts.Strength = 1 + 0; //default mage str + equipment str
            expectedAtts.Dexterity = 1 + 0; //default mage str + equipment str
            expectedAtts.Intelligence = 8 + 5; //default mage str + equipment str

            //Act
            mage.Equip(armor);
            actualAtts = mage.TotalAttributes();
            

            //Assert
            expectedAtts.ShouldDeepEqual(actualAtts);

        }

        [Fact]
        public void TotalAtt_TwoEquip_TestIfTotalAttributesAreCorrectWithTwoEquipment()
        {
            //Assemble
            Mage mage = new Mage("test");
            HeroAttribute actualAtts = new HeroAttribute();
            HeroAttribute expectedAtts = new HeroAttribute();
            Armor chestPiece = new Armor("common robes", Armor.ArmorType.Cloth, Item.Slot.Body, 0, 0, 5, 1);
            Armor headPiece = new Armor("common cap", Armor.ArmorType.Cloth, Item.Slot.Head, 0, 1, 3, 1);
            expectedAtts.Strength = 1 + 0 + 0; //default mage str + equipment str
            expectedAtts.Dexterity = 1 + 0 + 1; //default mage str + equipment str
            expectedAtts.Intelligence = 8 + 5 + 3; //default mage str + equipment str

            //Act
            mage.Equip(chestPiece);
            mage.Equip(headPiece);
            actualAtts = mage.TotalAttributes();


            //Assert
            expectedAtts.ShouldDeepEqual(actualAtts);

        }

        [Fact]
        public void TotalAtt_ReplacedEquip_TestIfTotalAttributesAreCorrectWithReplacedEquipment()
        {
            //Assemble
            Mage mage = new Mage("test");
            HeroAttribute actualAtts = new HeroAttribute();
            HeroAttribute expectedAtts = new HeroAttribute();
            Armor chestPiece = new Armor("common robes", Armor.ArmorType.Cloth, Item.Slot.Body, 0, 0, 5, 1);
            Armor headPiece = new Armor("common cap", Armor.ArmorType.Cloth, Item.Slot.Head, 0, 1, 3, 1);
            Armor betterChestPiece = new Armor("uncommon robes", Armor.ArmorType.Cloth, Item.Slot.Body, 1, 2, 6, 1);
            expectedAtts.Strength = 1 + 0 + 1; //default mage str + equipment str
            expectedAtts.Dexterity = 1 + 1 + 2; //default mage str + equipment str
            expectedAtts.Intelligence = 8 + 3 + 6; //default mage str + equipment str

            //Act
            mage.Equip(chestPiece);
            mage.Equip(headPiece);
            mage.Equip(betterChestPiece);
            actualAtts = mage.TotalAttributes();


            //Assert
            expectedAtts.ShouldDeepEqual(actualAtts);

        }
    }
}


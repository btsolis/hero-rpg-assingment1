﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assingment1_RPGHeroes.Equipment;
using Assingment1_RPGHeroes.Exceptions;
using Assingment1_RPGHeroes.NewFolder;
using DeepEqual;
using DeepEqual.Syntax;

namespace HeroRPGTest
{
    public class ItemEquip
    {
        [Fact]
        public void Item_CorrectWeaponEquip_ChecksIfWeaponsAreEquippedProperly()
        {
            //Assemble
            // we make a new mage and an accepetable weapon and see if it equips correctly
            Mage mage = new Mage("test");
            Weapon expectedWeapon = new Weapon("common wand", 5, Weapon.WeaponType.Wand, 1);

            //Act
            mage.Equip(expectedWeapon);

            //Assert
            expectedWeapon.ShouldDeepEqual(mage.EquippedWeapon);
        }

        [Fact]
        public void Item_CorrectArmorEquip_ChecksIfArmorssAreEquippedProperly()
        {
            //Assemble
            // we make a new mage and an accepetable armor and see if it equips correctly
            Mage mage = new Mage("test");
            Armor expectedArmor = new Armor("common robes", Armor.ArmorType.Cloth, Item.Slot.Body, 0, 0, 5, 1);

            //Act
            mage.Equip(expectedArmor);

            //Assert
            // we compare the armor we made with the appropriate equipment slot of the mage
            expectedArmor.ShouldDeepEqual(mage.Equipment[expectedArmor.EquipSlot]);
        }

        [Fact]
        public void Item_InvalidWeaponException_ChecksifInvalidWeaponExceptionIsThrown()
        {
            //Assemble
            // we make a new mage and an unaccepetable weapon(too high lvl req) and see if it throws the exception correctly
            Mage mage = new Mage("test");
            Weapon wrongWeapon = new Weapon("uncommon wand", 5, Weapon.WeaponType.Wand, 5);

            //Act
            var exception = Record.Exception(() => mage.Equip(wrongWeapon));


            //Assert
            Assert.IsType<InvalidWeaponException>(exception);

        }

        [Fact]
        public void Item_InvalidArmorException_ChecksifInvalidArmorExceptionIsThrown()
        {
            //Assemble
            // we make a new mage and an unaccepetable weapon(too high lvl req) and see if it throws the exception correctly
            Mage mage = new Mage("test");
            Armor wrongArmor = new Armor("common plate", Armor.ArmorType.Plate, Item.Slot.Body, 0, 0, 5, 1);

            //Act
            var exception = Record.Exception(() => mage.Equip(wrongArmor));


            //Assert
            Assert.IsType<InvalidArmorException>(exception);

        }
    }
}

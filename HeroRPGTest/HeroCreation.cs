using Assingment1_RPGHeroes.HeroClasses;
using Assingment1_RPGHeroes.NewFolder;

namespace HeroRPGTest;

public class HeroCreation
{//this class exists for testing the creation of various heroes 
    [Fact]
    public void Hero_CreateMage_ShouldHaveCorrectNameLevelandAtt()
    {
        //AAA
        //Assemble        
        int expectedLvl =1;
        int expectedStr =1;
        int expectedDex =1;
        int expectedInt =8;
        string expectedName = "Bill";

        //Act
        Mage mage = new Mage("Bill");

        //Assert
        // we cannot use Assert to compare object instances, hence we check each field individually.
        // in later tests, this is has been remedied by using the DeepEqual library which only looks at fields
        // and variables and doesn't compare the instances themselves
        Assert.Equal(mage.Name, expectedName);
        Assert.Equal(mage.Level, expectedLvl);
        Assert.Equal(mage.LevelAttributes.Intelligence, expectedInt);
        Assert.Equal(mage.LevelAttributes.Strength, expectedStr);
        Assert.Equal(mage.LevelAttributes.Dexterity, expectedDex);

    }

    public void Hero_CreateRAnger_ShouldHaveCorrectNameLevelandAtt()
    {
        //AAA
        //Assemble        
        int expectedLvl = 1;
        int expectedStr = 1;
        int expectedDex = 7;
        int expectedInt = 1;
        string expectedName = "Bill";

        //Act
        Ranger ranger = new Ranger("Bill");

        //Assert
        // we cannot use Assert to compare object instances, hence we check each field individually.
        // in later tests, this is has been remedied by using the DeepEqual library which only looks at fields
        // and variables and doesn't compare the instances themselves
        Assert.Equal(ranger.Name, expectedName);
        Assert.Equal(ranger.Level, expectedLvl);
        Assert.Equal(ranger.LevelAttributes.Intelligence, expectedInt);
        Assert.Equal(ranger.LevelAttributes.Strength, expectedStr);
        Assert.Equal(ranger.LevelAttributes.Dexterity, expectedDex);

    }

    public void Hero_CreateRogue_ShouldHaveCorrectNameLevelandAtt()
    {
        //AAA
        //Assemble        
        int expectedLvl = 1;
        int expectedStr = 2;
        int expectedDex = 6;
        int expectedInt = 1;
        string expectedName = "Bill";

        //Act
        Rogue rogue = new Rogue("Bill");

        //Assert
        // we cannot use Assert to compare object instances, hence we check each field individually.
        // in later tests, this is has been remedied by using the DeepEqual library which only looks at fields
        // and variables and doesn't compare the instances themselves
        Assert.Equal(rogue.Name, expectedName);
        Assert.Equal(rogue.Level, expectedLvl);
        Assert.Equal(rogue.LevelAttributes.Intelligence, expectedInt);
        Assert.Equal(rogue.LevelAttributes.Strength, expectedStr);
        Assert.Equal(rogue.LevelAttributes.Dexterity, expectedDex);

    }

    public void Hero_CreateWarrior_ShouldHaveCorrectNameLevelandAtt()
    {
        //AAA
        //Assemble        
        int expectedLvl = 1;
        int expectedStr = 5;
        int expectedDex = 2;
        int expectedInt = 1;
        string expectedName = "Bill";

        //Act
        Warrior warrior= new Warrior("Bill");

        //Assert
        // we cannot use Assert to compare object instances, hence we check each field individually.
        // in later tests, this is has been remedied by using the DeepEqual library which only looks at fields
        // and variables and doesn't compare the instances themselves
        Assert.Equal(warrior.Name, expectedName);
        Assert.Equal(warrior.Level, expectedLvl);
        Assert.Equal(warrior.LevelAttributes.Intelligence, expectedInt);
        Assert.Equal(warrior.LevelAttributes.Strength, expectedStr);
        Assert.Equal(warrior.LevelAttributes.Dexterity, expectedDex);

    }


}
﻿using Assingment1_RPGHeroes.Equipment;
using DeepEqual.Syntax;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroRPGTest
{
    public class ItemCreation
    {
        [Fact]

        public void Item_MakeWeapon_ShouldHaveCorrectValues()
        {
            //Assemble  
            string expectedName = "Wand of Miracles";
            int expectedDmg = 30;
            Item.Slot expectedSlot = Item.Slot.Weapon;
            Weapon.WeaponType expectedType = Weapon.WeaponType.Wand;
            int expectedReqLvl = 5;
            Weapon expectedWeapon = new Weapon(expectedName, expectedDmg, expectedType, expectedReqLvl);
            expectedWeapon.Name= expectedName;
            expectedWeapon.WeaponDamage = expectedDmg;
            expectedWeapon.EquipSlot = expectedSlot;
            expectedWeapon.Type = expectedType;
            expectedWeapon.RequiredLevel= expectedReqLvl;


            //Act
            Weapon actualWeapon = new Weapon(expectedName, expectedDmg, expectedType, expectedReqLvl);


            //Assert
            actualWeapon.ShouldDeepEqual(expectedWeapon);

        }


        [Fact]
        public void Item_MakeArmor_ShouldHaveCorrectValues()
        {
            //Assemble  
            string expectedName = "Smart Robes";
            int expectedStr = 1;
            int expectedDex = 4;
            int expectedInt = 10;
            Item.Slot slot = Armor.Slot.Body;
            Armor.ArmorType type = Armor.ArmorType.Cloth;
            int expectedReqLvl = 5;

            //Act
            Armor robes = new(expectedName, type, slot, expectedStr, expectedDex,expectedInt, expectedReqLvl);

            //Assert
            Assert.Equal(robes.ArmorAttribute.Strength, expectedStr);
            Assert.Equal(robes.ArmorAttribute.Dexterity, expectedDex);
            Assert.Equal(robes.ArmorAttribute.Intelligence, expectedInt);
            Assert.Equal(robes.Name, expectedName);
            Assert.Equal(robes.RequiredLevel, expectedReqLvl);
            Assert.Equal(robes.EquipSlot, slot);
            Assert.Equal(robes.Type, type);


        }

    }
}

﻿using Assingment1_RPGHeroes;
using Assingment1_RPGHeroes.HeroClasses;
using Assingment1_RPGHeroes.NewFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroRPGTest
{
    public class HeroLevelUp
    {//this class exists for testing leveling up and attribute increases/changes for various heroes

        [Theory]
        [InlineData(4 ,5, 6)]
        [InlineData(10, 10, 10)]
        [InlineData(-20, 0, -5)]
        public void Attributes_IncreaseAttributes_ShouldIncreaseHeroAttributesCorrectly(int strInc,int dexInc,int intInc)
        {
            //Assemble
            HeroAttribute heroAtt = new HeroAttribute();
            heroAtt.Strength = 4;
            heroAtt.Dexterity = 12;
            heroAtt.Intelligence = 6;
            
            int finalStr = heroAtt.Strength + strInc;   // 4 + 4
            int finalDex = heroAtt.Dexterity + dexInc ;  // 12 + 5
            int finalInt = heroAtt.Intelligence + intInc;  // 6 + 6       

            //Act            
            heroAtt.ChangeAtts(strInc, dexInc, intInc);

            //Assert
            Assert.Equal(heroAtt.Strength, finalStr);
            Assert.Equal(heroAtt.Dexterity, finalDex);
            Assert.Equal(heroAtt.Intelligence, finalInt);

            
        }

        [Theory]
        [InlineData(1)]
        [InlineData(5)]
        [InlineData(22)]

        public void Hero_LvlUpMage_ShouldLvlUpMagesAttsCorrectly(int lvlUpAmount)
        {
            //AAA
            //Assemble 
            // default mage attributes = 1 , 1 , 8
            int expectedLvl = 1 + (1 * lvlUpAmount);
            int expectedStr = 1 + (1 * lvlUpAmount);
            int expectedDex = 1 + (1 * lvlUpAmount);
            int expectedInt = 8 + (5 * lvlUpAmount);



            
            //Act
            Mage mage = new Mage("Bill");
            for (int i = 0; i < lvlUpAmount; i++) { mage.MageLevel(); }


            //Assert
            Assert.True(mage.Level == expectedLvl);
            Assert.Equal(mage.LevelAttributes.Intelligence, expectedInt);
            Assert.Equal(mage.LevelAttributes.Strength, expectedStr);
            Assert.Equal(mage.LevelAttributes.Dexterity, expectedDex);

        }

        [Theory]
        [InlineData(1)]
        [InlineData(5)]
        [InlineData(22)]

        public void Hero_LvlUpRanger_ShouldLvlUpRangersAttsCorrectly(int lvlUpAmount)
        {
            //AAA
            //Assemble 
            // default mage attributes = 1 , 7 , 1
            int expectedLvl = 1 + (1 * lvlUpAmount);
            int expectedStr = 1 + (1 * lvlUpAmount);
            int expectedDex = 7 + (5 * lvlUpAmount);
            int expectedInt = 1 + (1 * lvlUpAmount);




            //Act
            Ranger ranger = new Ranger("Bill");
            for (int i = 0; i < lvlUpAmount; i++) { ranger.RangerLevel(); }


            //Assert
            Assert.True(ranger.Level == expectedLvl);
            Assert.Equal(ranger.LevelAttributes.Intelligence, expectedInt);
            Assert.Equal(ranger.LevelAttributes.Strength, expectedStr);
            Assert.Equal(ranger.LevelAttributes.Dexterity, expectedDex);

        }

        [Theory]
        [InlineData(1)]
        [InlineData(5)]
        [InlineData(22)]

        public void Hero_LvlUpRogue_ShouldLvlUpRoguessAttsCorrectly(int lvlUpAmount)
        {
            //AAA
            //Assemble 
            // default mage attributes = 2 , 6 , 1
            int expectedLvl = 1 + (1 * lvlUpAmount);
            int expectedStr = 2 + (1 * lvlUpAmount);
            int expectedDex = 6 + (4 * lvlUpAmount);
            int expectedInt = 1 + (1 * lvlUpAmount);




            //Act
            Rogue rogue = new Rogue("Bill");
            for (int i = 0; i < lvlUpAmount; i++) { rogue.RogueLevel(); }


            //Assert
            Assert.True(rogue.Level == expectedLvl);
            Assert.Equal(rogue.LevelAttributes.Intelligence, expectedInt);
            Assert.Equal(rogue.LevelAttributes.Strength, expectedStr);
            Assert.Equal(rogue.LevelAttributes.Dexterity, expectedDex);

        }


        [Theory]
        [InlineData(1)]
        [InlineData(5)]
        [InlineData(22)]

        public void Hero_LvlUpWarrior_ShouldLvlUpWarriorsAttsCorrectly(int lvlUpAmount)
        {
            //AAA
            //Assemble 
            // default mage attributes = 5 , 2 , 1
            int expectedLvl = 1 + (1 * lvlUpAmount);
            int expectedStr = 5 + (3 * lvlUpAmount);
            int expectedDex = 2 + (2 * lvlUpAmount);
            int expectedInt = 1 + (1 * lvlUpAmount);




            //Act
            Warrior warrior = new Warrior("Bill");
            for (int i = 0; i < lvlUpAmount; i++) { warrior.WarriorLevel(); }


            //Assert
            Assert.True(warrior.Level == expectedLvl);
            Assert.Equal(warrior.LevelAttributes.Intelligence, expectedInt);
            Assert.Equal(warrior.LevelAttributes.Strength, expectedStr);
            Assert.Equal(warrior.LevelAttributes.Dexterity, expectedDex);

        }
    }
}

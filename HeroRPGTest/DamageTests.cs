﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assingment1_RPGHeroes.Equipment;
using Assingment1_RPGHeroes.NewFolder;
using DeepEqual;

namespace HeroRPGTest
{
    public class DamageTests
    {
        [Fact]
        public void Damage_NoWep_ChecksIfCorrectDmgWithoutWeapons()
        {
            //Assemble
            Mage mage = new Mage("test");
            int excectedDmg = 1 * (1 + 8 / 100);// wep dmg(1) + (1 + dmg att(int)/100)

            //Act
            int actualDmg = mage.Damage();

            //Assert
            Assert.Equal(excectedDmg, actualDmg);
        }
        [Fact]
        public void Damage_Wep_ChecksIfCorrectDmgWithWeapon()
        {
            //Assemble
            Mage mage = new Mage("test");
            Weapon weapon = new Weapon("common wand", 5, Weapon.WeaponType.Wand, 1);
            int excectedDmg = 5 * (1 + 8 / 100);// wep dmg(5) + (1 + dmg att(int)/100)

            //Act
            mage.Equip(weapon);
            int actualDmg = mage.Damage();

            //Assert
            Assert.Equal(excectedDmg, actualDmg);
        }

        [Fact]
        public void Damage_WepAndArmor_ChecksIfCorrectDmgWithWepandArmor()
        {
            //Assemble
            Mage mage = new Mage("test");            
            Weapon weapon = new Weapon("common wand", 5, Weapon.WeaponType.Wand, 1);
            Armor betterChestPiece = new Armor("uncommon robes", Armor.ArmorType.Cloth, Item.Slot.Body, 1, 2, 6, 1);
            int excectedDmg = 5 * (1 + 14 / 100);// wep dmg(5) + (1 + dmg att(int)/100)

            //Act
            mage.Equip(weapon);
            mage.Equip(betterChestPiece);
            int actualDmg = mage.Damage();

            //Assert
            Assert.Equal(excectedDmg, actualDmg);
        }

        [Fact]
        public void Damage_ReplacedWepAndArmor_ChecksIfCorrectDmgWithReplacedWepandArmor()
        {
            //Assemble
            Mage mage = new Mage("test");
            Weapon weapon = new Weapon("common wand", 5, Weapon.WeaponType.Wand, 1);
            Weapon betterWeapon = new Weapon("common staff", 8, Weapon.WeaponType.Staff, 1);
            Armor betterChestPiece = new Armor("uncommon robes", Armor.ArmorType.Cloth, Item.Slot.Body, 1, 2, 6, 1);
            int excectedDmg = 8 * (1 + 14 / 100);// wep dmg(5) + (1 + dmg att(int)/100)

            //Act
            mage.Equip(weapon);
            mage.Equip(betterChestPiece);
            mage.Equip(betterWeapon);
            int actualDmg = mage.Damage();

            //Assert
            Assert.Equal(excectedDmg, actualDmg);
        }
    }
}
